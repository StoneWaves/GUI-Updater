package com.stonewaves.trials;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


class config_trial_1 {

    public static void main(String[] args) throws IOException{
        String LogPath = ".\\LogFiles\\";
        String CloudPath = "D:\\Sync Test Folders\\Cloud Configs";
        String InstancePath = "D:\\Sync Test Folders\\Instance Configs";

        //Lists for comparisons
        ArrayList CloudConfigFiles = new ArrayList( );
        ArrayList CloudConfigTimes = new ArrayList<Date>( );
        ArrayList InstanceConfigFiles = new ArrayList( );
        ArrayList InstanceConfigTimes = new ArrayList<Date>( );
        ArrayList OutDatedFromInstance = new ArrayList();
        ArrayList NewConfigFromCopy = new ArrayList();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");



        //Generates list of configs in cloud
        cloudConfig(CloudPath, CloudConfigFiles, CloudConfigTimes, sdf);

        //Generate list of configs in instance
        instanceConfigs(InstancePath, InstanceConfigFiles, InstanceConfigTimes, sdf);

        //Adds missing configs to instance
        addNewConfigs(LogPath, CloudPath, InstancePath, InstanceConfigFiles, NewConfigFromCopy);

        //Clear Instance file data and recalculate
        InstanceConfigFiles.clear();
        InstanceConfigTimes.clear();

        //Generate list of configs in instance after files have been added
        instanceConfigs(InstancePath, InstanceConfigFiles, InstanceConfigTimes, sdf);

        //Finds the files that have been updated
        compareTimes(CloudConfigFiles, InstanceConfigFiles, CloudConfigTimes, InstanceConfigTimes, InstancePath, CloudPath);



        //Debug attach line
        System.out.println("This marks the end of the program and allows for the debugger to attach");
    }

    private static void cloudConfig(String cloudPath, ArrayList cloudConfigFiles, ArrayList cloudConfigTimes, SimpleDateFormat sdf) {
        String files;
        File folder = new File(cloudPath);
        File[] listOfFiles = folder.listFiles();

        int count = 0;
        for (File listOfFile : listOfFiles != null ? listOfFiles : new File[0]) {
            if (listOfFile.isFile()) {
                files = listOfFile.getName();

                cloudConfigFiles.add(files);
                cloudConfigTimes.add(listOfFile.lastModified());
                count ++;
            }
        }

        System.out.println(cloudConfigFiles.size() + " file(s) in location cloud folder");
    }

    private static void instanceConfigs(String instancePath, ArrayList instanceConfigFiles, ArrayList instanceConfigTimes, SimpleDateFormat sdf) {
        String files;
        File folder = new File(instancePath);
        File[] listOfFiles = folder.listFiles();


        int count = 0;
        for (File listOfFile : listOfFiles != null ? listOfFiles : new File[0]) {
            if (listOfFile.isFile()) {
                files = listOfFile.getName();

                //System.out.println(files);
                instanceConfigFiles.add(files);
                instanceConfigTimes.add(listOfFile.lastModified());

                //System.out.println(InstanceConfigFiles.get(count));
                //System.out.println(InstanceConfigTimes.get(count));
                count ++;
                //System.out.println("Update time : " + sdf.format(listOfFile.lastModified()));
            }
        }

        System.out.println(instanceConfigFiles.size() + " file(s) in location cloud folder");
    }

    private static void addNewConfigs(String logPath, String cloudPath, String instancePath, ArrayList instanceConfigFiles, ArrayList newConfigFromCopy) throws IOException {
        String files;
        File folder = new File(cloudPath);
        File[] listOfFiles = folder.listFiles();
        //String NameMissing = logPath + "New files.txt";


        for (File listOfFile : listOfFiles != null ? listOfFiles : new File[0]) {
            if (listOfFile.isFile()) {
                files = listOfFile.getName();
                if (instanceConfigFiles.contains(files)) {
                    //Do nothing
                } else {
                    CopyOption[]
                            options = new CopyOption[]{
                            StandardCopyOption.REPLACE_EXISTING,
                            StandardCopyOption.COPY_ATTRIBUTES
                    };

                    Path From = Paths.get(cloudPath + "\\" + files);
                    Path To = Paths.get(instancePath + "\\" + files);
                    Files.copy(From, To, options);
                    //System.out.println("Added : " + files);
                    newConfigFromCopy.add(files);

                }
            }
        }
    }

    private static void compareTimes(ArrayList cloudConfigFiles, ArrayList instanceConfigFiles,  ArrayList cloudConfigTimes, ArrayList instanceConfigTimes, String InstancePath, String CloudPath) throws IOException {

        for(int count = 0; cloudConfigFiles.size() > count; count++ )
            if (cloudConfigTimes.get(count).equals(instanceConfigTimes.get(count))) {

            } else {
                String cloud = (String) cloudConfigFiles.get(count);

                //compare times
                for (int x = 0; x < cloudConfigTimes.size(); x++) {
                    long cloudTime = (Long) (cloudConfigTimes.get(x));
                    long instanceTime = (Long) (instanceConfigTimes.get(x));

                    if (cloudTime > instanceTime) {
                        instanceConfigTimes.set(x, cloudTime);
                        System.out.println("Cloud is newer, update " + cloud);
                        //TODO call update script and pass file

                        //Delete file from instance
                        Files.delete(Paths.get(InstancePath + "\\" + cloud));



                        //Add file from copy to instance
                        CopyOption[]
                                options = new CopyOption[]{
                                StandardCopyOption.REPLACE_EXISTING,
                                StandardCopyOption.COPY_ATTRIBUTES
                        };

                        Path From = Paths.get(CloudPath + "\\" + cloud);
                        Path To = Paths.get(InstancePath + "\\" + cloud);
                        Files.copy(From, To, options);

                    }

                }


            }

    }


}
