package com.stonewaves.updater;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class Rewrite extends Application {

    private String CloudPath = "";
    private String InstancePath = "";

    @Override
    public void start(final Stage primaryStage) {

        //Window setup
        primaryStage.setTitle("Client Updater");
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(25, 25, 25, 25));
        Scene scene = new Scene(pane, 700, 275);

        //--------------------------------------------------------------------------------------------------------------

        //Add the title
        Text sceneTitle = new Text("Client Updater");
        sceneTitle.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
        pane.add(sceneTitle, 0, 0, 2, 1);

        //--------------------------------------------------------------------------------------------------------------

        //Show the starting pane
        StartignButtons(pane, primaryStage);

        //--------------------------------------------------------------------------------------------------------------

        //Show everything
        primaryStage.setScene(scene);
        primaryStage.show();


    }

    //This is the pane shows on startup
    private void StartignButtons(final GridPane pane, final Stage primaryStage) {
        //Mod Button display stuff
        final Button modsButton = new Button("Mods!");
        HBox modhbox = new HBox(10);

        modhbox.setAlignment(Pos.BOTTOM_LEFT);
        modhbox.getChildren().add(modsButton);
        pane.add(modhbox, 1, 4);

        //--------------------------------------------------------------------------------------------------------------

        //Config Button display stuff
        final Button configButton = new Button("Configs!");
        HBox confighbox = new HBox(10);

        confighbox.setAlignment(Pos.BOTTOM_LEFT);
        confighbox.getChildren().add(configButton);
        pane.add(confighbox, 2, 4);

        //--------------------------------------------------------------------------------------------------------------

        //Mods Button action stuff
        modsButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                System.out.println("Mods btn clicked");
                //hide the buttons in prep for the new pane
                configButton.setVisible(false);
                modsButton.setVisible(false);
                ModsPane(pane, primaryStage);
            }
        });

        //--------------------------------------------------------------------------------------------------------------

        //Config Button action stuff
        configButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                System.out.println("Configs btn clicked");
                //hide the buttons in prep for the new pane
                configButton.setVisible(false);
                modsButton.setVisible(false);
                ConfigsPane(pane, primaryStage);
            }
        });
    }

    //This pane allows for updating the mods folder
    private void ModsPane(final GridPane pane, final Stage primaryStage){

        //Copy Directory Picker code
        final Text labelCopyDirectory = new Text();
        labelCopyDirectory.setText("No Directory selected");

        final Button btnOpenCopyDirectoryChooser = new Button();

        btnOpenCopyDirectoryChooser.setText("Select Directory");
        btnOpenCopyDirectoryChooser.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DirectoryChooser directoryChooser = new DirectoryChooser();
                File selectedDirectory =
                        directoryChooser.showDialog(primaryStage);

                if (selectedDirectory == null) {
                    labelCopyDirectory.setText("No Directory selected");
                } else {
                    labelCopyDirectory.setText(selectedDirectory.getAbsolutePath());
                    CloudPath = selectedDirectory.getAbsolutePath();
                }
            }
        });

        //--------------------------------------------------------------------------------------------------------------

        //Instance Directory Picker code
        final Text labelInstanceDirectory = new Text();
        labelInstanceDirectory.setText("No Directory selected");

        final Button btnOpenInstanceDirectoryChooser = new Button();

        btnOpenInstanceDirectoryChooser.setText("Select Directory");
        btnOpenInstanceDirectoryChooser.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DirectoryChooser directoryChooser = new DirectoryChooser();
                File selectedDirectory =
                        directoryChooser.showDialog(primaryStage);

                if (selectedDirectory == null) {
                    labelInstanceDirectory.setText("No Directory selected");
                } else {
                    labelInstanceDirectory.setText(selectedDirectory.getAbsolutePath());
                    InstancePath = selectedDirectory.getAbsolutePath();
                }
            }
        });

        //--------------------------------------------------------------------------------------------------------------

        //Add the title
        Text sceneTitle = new Text("Client Updater");
        sceneTitle.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
        pane.add(sceneTitle, 0, 0, 2, 1);

        //--------------------------------------------------------------------------------------------------------------

        //Add the Copy display stuff
        final Text Copy = new Text("Copy mods path");
        Copy.setFont(Font.font("Arial", FontWeight.NORMAL, 14));
        pane.add(Copy, 1, 1);

        //--------------------------------------------------------------------------------------------------------------

        pane.add(btnOpenCopyDirectoryChooser, 2, 1);
        pane.add(labelCopyDirectory, 3, 1);

        //--------------------------------------------------------------------------------------------------------------

        //Add the Instance display stuff
        final Text Instance = new Text("Instance mods path");
        Instance.setFont(Font.font("Arial", FontWeight.NORMAL, 14));
        pane.add(Instance, 1, 2);

        pane.add(btnOpenInstanceDirectoryChooser, 2, 2);
        pane.add(labelInstanceDirectory, 3, 2);

        //--------------------------------------------------------------------------------------------------------------

        //Code for the Update Button item

        //Update Button display stuff
        Button updateButton = new Button("Update!");
        final HBox hbox = new HBox(10);

        hbox.setAlignment(Pos.BOTTOM_LEFT);
        hbox.getChildren().add(updateButton);
        pane.add(hbox, 1, 4);

        final Text updateMessage = new Text();
        pane.add(updateMessage, 1, 6);

        //--------------------------------------------------------------------------------------------------------------

        //Update Button action stuff
        updateButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {

                if (CloudPath.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Cloud path not declared");
                } else if (InstancePath.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Instance path not declared");
                } else {
                    try {
                        ModsCode.runMod(CloudPath, InstancePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //runUpdate(CloudPath, InstancePath);
                    System.out.println("Update btn click successful");
                    updateMessage.setText("Updated");
                }
            }
        });

        //--------------------------------------------------------------------------------------------------------------

        //Code for the Home Button item

        //Update Button display stuff
        Button HomeButton = new Button("Home");
        final HBox homebox = new HBox(10);

        homebox.setAlignment(Pos.BOTTOM_LEFT);
        homebox.getChildren().add(HomeButton);
        pane.add(homebox, 2, 4);

        //--------------------------------------------------------------------------------------------------------------

        //Update Button action stuff
        HomeButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                //Code to hide all elements
                labelCopyDirectory.setVisible(false);
                btnOpenCopyDirectoryChooser.setVisible(false);
                labelInstanceDirectory.setVisible(false);
                btnOpenInstanceDirectoryChooser.setVisible(false);
                Copy.setVisible(false);
                Instance.setVisible(false);
                hbox.setVisible(false);
                homebox.setVisible(false);

                //Show home pane again
                StartignButtons(pane, primaryStage);
            }
        });
    }

    //This pane allows for updating the configs folder
    private void ConfigsPane(final GridPane pane, final Stage primaryStage){

        //Copy Directory Picker code
        final Text labelCopyDirectory1 = new Text();
        labelCopyDirectory1.setText("No Directory selected");

        final Button btnOpenCopyDirectoryChooser1 = new Button();

        btnOpenCopyDirectoryChooser1.setText("Select Directory");
        btnOpenCopyDirectoryChooser1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DirectoryChooser directoryChooser1 = new DirectoryChooser();
                File selectedDirectory1 =
                        directoryChooser1.showDialog(primaryStage);

                if (selectedDirectory1 == null) {
                    labelCopyDirectory1.setText("No Directory selected");
                } else {
                    labelCopyDirectory1.setText(selectedDirectory1.getAbsolutePath());
                    CloudPath = selectedDirectory1.getAbsolutePath();
                }
            }
        });

        //--------------------------------------------------------------------------------------------------------------

        //Instance Directory Picker code
        final Text labelInstanceDirectory1 = new Text();
        labelInstanceDirectory1.setText("No Directory selected");

        final Button btnOpenInstanceDirectoryChooser1 = new Button();

        btnOpenInstanceDirectoryChooser1.setText("Select Directory");
        btnOpenInstanceDirectoryChooser1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DirectoryChooser directoryChooser1 = new DirectoryChooser();
                File selectedDirectory =
                        directoryChooser1.showDialog(primaryStage);

                if (selectedDirectory == null) {
                    labelInstanceDirectory1.setText("No Directory selected");
                } else {
                    labelInstanceDirectory1.setText(selectedDirectory.getAbsolutePath());
                    InstancePath = selectedDirectory.getAbsolutePath();
                }
            }
        });

        //--------------------------------------------------------------------------------------------------------------

        //Add the title
        Text sceneTitle1 = new Text("Client Updater");
        sceneTitle1.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
        pane.add(sceneTitle1, 0, 0, 2, 1);

        //--------------------------------------------------------------------------------------------------------------

        //Add the Copy display stuff
        final Text Copy1 = new Text("Cloud configs path");
        Copy1.setFont(Font.font("Arial", FontWeight.NORMAL, 14));
        pane.add(Copy1, 1, 1);

        //--------------------------------------------------------------------------------------------------------------

        pane.add(btnOpenCopyDirectoryChooser1, 2, 1);
        pane.add(labelCopyDirectory1, 3, 1);

        //--------------------------------------------------------------------------------------------------------------

        //Add the Instance display stuff
        final Text Instance = new Text("Instance configs path");
        Instance.setFont(Font.font("Arial", FontWeight.NORMAL, 14));
        pane.add(Instance, 1, 2);

        pane.add(btnOpenInstanceDirectoryChooser1, 2, 2);
        pane.add(labelInstanceDirectory1, 3, 2);

        //--------------------------------------------------------------------------------------------------------------

        //Code for the Update Button item

        //Update Button display stuff
        Button updateButton = new Button("Update!");
        final HBox hbox1 = new HBox(10);

        hbox1.setAlignment(Pos.BOTTOM_LEFT);
        hbox1.getChildren().add(updateButton);
        pane.add(hbox1, 1, 4);

        final Text updateMessage = new Text();
        pane.add(updateMessage, 1, 6);

        //--------------------------------------------------------------------------------------------------------------

        //Update Button action stuff
        updateButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {

                if (CloudPath.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Cloud path not declared");
                } else if (InstancePath.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Instance path not declared");
                } else {
                    try {
                        ConfigCode.runConfig(CloudPath, InstancePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //runUpdate(CloudPath, InstancePath);
                    System.out.println("Update btn click successful");
                    updateMessage.setText("Updated");
                }
            }
        });

        //--------------------------------------------------------------------------------------------------------------

        //Code for the Home Button item

        //Update Button display stuff
        Button HomeButton = new Button("Home");
        final HBox homebox = new HBox(10);

        homebox.setAlignment(Pos.BOTTOM_LEFT);
        homebox.getChildren().add(HomeButton);
        pane.add(homebox, 2, 4);

        //--------------------------------------------------------------------------------------------------------------

        //Update Button action stuff
        HomeButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                //Code to hide all the things
                labelCopyDirectory1.setVisible(false);
                btnOpenCopyDirectoryChooser1.setVisible(false);
                labelInstanceDirectory1.setVisible(false);
                btnOpenInstanceDirectoryChooser1.setVisible(false);
                Copy1.setVisible(false);
                Instance.setVisible(false);
                hbox1.setVisible(false);
                homebox.setVisible(false);

                //Show home pane again
                StartignButtons(pane, primaryStage);
            }
        });
    }


}
