package com.stonewaves.updater;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;

public class Main extends Application {

    private String CloudPath = "";
    private String InstancePath = "";

    @Override
    public void start(final Stage primaryStage) {

        //Window setup
        primaryStage.setTitle("Client Updater");
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(25, 25, 25, 25));
        Scene scene = new Scene(pane, 700, 275);

        //--------------------------------------------------------------------------------------------------------------

        //Copy Directory Picker code
        final Text labelCopyDirectory = new Text();
        labelCopyDirectory.setText("No Directory selected");

        Button btnOpenCopyDirectoryChooser = new Button();

        btnOpenCopyDirectoryChooser.setText("Select Directory");
        btnOpenCopyDirectoryChooser.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DirectoryChooser directoryChooser = new DirectoryChooser();
                File selectedDirectory =
                        directoryChooser.showDialog(primaryStage);

                if (selectedDirectory == null) {
                    labelCopyDirectory.setText("No Directory selected");
                } else {
                    labelCopyDirectory.setText(selectedDirectory.getAbsolutePath());
                    CloudPath = selectedDirectory.getAbsolutePath();
                }
            }
        });

        //--------------------------------------------------------------------------------------------------------------

        //Instance Directory Picker code
        final Text labelInstanceDirectory = new Text();
        labelInstanceDirectory.setText("No Directory selected");

        Button btnOpenInstanceDirectoryChooser = new Button();

        btnOpenInstanceDirectoryChooser.setText("Select Directory");
        btnOpenInstanceDirectoryChooser.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DirectoryChooser directoryChooser = new DirectoryChooser();
                File selectedDirectory =
                        directoryChooser.showDialog(primaryStage);

                if (selectedDirectory == null) {
                    labelInstanceDirectory.setText("No Directory selected");
                } else {
                    labelInstanceDirectory.setText(selectedDirectory.getAbsolutePath());
                    InstancePath = selectedDirectory.getAbsolutePath();
                }
            }
        });

        //--------------------------------------------------------------------------------------------------------------

        //Add the title
        Text sceneTitle = new Text("Client Updater");
        sceneTitle.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
        pane.add(sceneTitle, 0, 0, 2, 1);

        //--------------------------------------------------------------------------------------------------------------

        //Add the Copy display stuff
        Text Copy = new Text("Copy mods path");
        Copy.setFont(Font.font("Arial", FontWeight.NORMAL, 14));
        pane.add(Copy, 1, 1);

        //--------------------------------------------------------------------------------------------------------------

        pane.add(btnOpenCopyDirectoryChooser, 2, 1);
        pane.add(labelCopyDirectory, 3, 1);

        //--------------------------------------------------------------------------------------------------------------

        //Add the Instance display stuff
        Text Instance = new Text("Instance mods path");
        Instance.setFont(Font.font("Arial", FontWeight.NORMAL, 14));
        pane.add(Instance, 1, 2);

        pane.add(btnOpenInstanceDirectoryChooser, 2, 2);
        pane.add(labelInstanceDirectory, 3, 2);

        //--------------------------------------------------------------------------------------------------------------

        //Add the update button
        updateButton(pane);

        //--------------------------------------------------------------------------------------------------------------

        //Add the Client Side button
        clientSideButton(pane);

        //--------------------------------------------------------------------------------------------------------------

        //Add the Log Files button
        logFilesButton(pane);

        //--------------------------------------------------------------------------------------------------------------

        //Show everything
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    //Code for the Update Button item
    void updateButton(GridPane pane) {
        //Update Button display stuff
        Button updateButton = new Button("Update!");
        HBox hbox = new HBox(10);

        hbox.setAlignment(Pos.BOTTOM_LEFT);
        hbox.getChildren().add(updateButton);
        pane.add(hbox, 1, 4);

        final Text updateMessage = new Text();
        pane.add(updateMessage, 1, 6);

        //--------------------------------------------------------------------------------------------------------------

        //Update Button action stuff
        updateButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                try {

                    if (CloudPath.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "Cloud path not declared");
                    } else if (InstancePath.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "Instance path not declared");
                    } else {
                        runUpdate(CloudPath, InstancePath);
                        updateMessage.setText("Updated");
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    updateMessage.setText(e.getLocalizedMessage());
                    //updateMessage.setText("Something went wrong!");
                }

            }
        });

    }

    //Code for the Client Side mods button item
    void clientSideButton(GridPane pane) {
        //Clientsides button display stuff
        Button clientsidesButton = new Button("Client Side Mods");
        HBox hboxClientsides = new HBox(10);

        hboxClientsides.setAlignment(Pos.BOTTOM_RIGHT);
        hboxClientsides.getChildren().add(clientsidesButton);
        pane.add(hboxClientsides, 2, 4);

        //--------------------------------------------------------------------------------------------------------------

        //Action code
        clientsidesButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {

                try {
                    findDirectory();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Desktop dt = Desktop.getDesktop();
                try {
                    dt.open(new File(".\\LogFiles\\clientsides.txt"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //Code for Log Files button item
    void logFilesButton(GridPane pane) {
        //Log Files button display stuff
        Button logFilesButton = new Button("Log Files");
        HBox hboxLogFiles = new HBox(10);

        hboxLogFiles.setAlignment(Pos.BOTTOM_RIGHT);
        hboxLogFiles.getChildren().add(logFilesButton);
        pane.add(hboxLogFiles, 3, 4);

        //--------------------------------------------------------------------------------------------------------------

        //Action code
        logFilesButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {

                try {
                    findDirectory();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Desktop dt = Desktop.getDesktop();
                try {
                    dt.open(new File(".\\LogFiles"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //Runs need methods for updating selected folder
    private static void runUpdate(String CloudPath, String InstancePath) throws IOException {

        String LogPath = ".\\LogFiles\\";

        //Lists for comparisons
        ArrayList CloudFiles = new ArrayList();
        ArrayList InstanceFiles = new ArrayList();
        ArrayList Approved = new ArrayList();
        ArrayList MarkedForRemoval = new ArrayList();
        ArrayList RemovalApproved = new ArrayList();
        ArrayList NewFiles = new ArrayList();


        //--------------------------------------------------------------------------------------------------------------

        //Check for log Directory & create if it doesn't exist
        findDirectory();

        //--------------------------------------------------------------------------------------------------------------

        //Add files from Copy to the 'CloudFiles' Array List
        cloud(CloudFiles, CloudPath);

        //--------------------------------------------------------------------------------------------------------------

        //Add files from the selected to the 'InstanceFiles' Array List
        instance(InstanceFiles, InstancePath);

        //--------------------------------------------------------------------------------------------------------------

        //Generate a list of up-to-date file along with those that are not in the Copy
        missingFromCopy(CloudFiles, InstancePath, LogPath, MarkedForRemoval);

        //--------------------------------------------------------------------------------------------------------------

        //Generate list of new files in Copy
        addToInstance(InstanceFiles, CloudPath, InstancePath, LogPath, NewFiles);

        //--------------------------------------------------------------------------------------------------------------

        //Remove files
        fileRemoval(InstancePath, LogPath, Approved, MarkedForRemoval, RemovalApproved, LogPath);

    }

    //Check for log Directory & create if it doesn't exist
    private static void findDirectory() throws IOException {

        File dir = new File(".\\LogFiles");
        new File(".\\LogFiles\\").mkdirs();

        new File(".\\LogFiles\\Clientsides.txt").createNewFile();
        new File(".\\LogFiles\\New files.txt").createNewFile();
        new File(".\\LogFiles\\Up to date files.txt").createNewFile();
        new File(".\\LogFiles\\Removed files.txt").createNewFile();

    }

    //Add files from Copy to the 'CloudFiles' Array List
    private static void cloud(ArrayList listA, String path) {
        String files;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (File listOfFile : listOfFiles != null ? listOfFiles : new File[0]) {
            if (listOfFile.isFile()) {
                files = listOfFile.getName();
                //System.out.println(files);
                listA.add(files);
            }
        }
        System.out.println(listA.size() + " file(s) in location cloud folder");
    }

    //Add files from the selected to the 'InstanceFiles' Array List
    private static void instance(ArrayList listB, String path) {
        String files;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (File listOfFile : listOfFiles != null ? listOfFiles : new File[0]) {
            if (listOfFile.isFile()) {
                files = listOfFile.getName();
                listB.add(files);
            }
        }
        System.out.println(listB.size() + " file(s) in location instance folder");
    }

    //Generate a list of up-to-date file along with those that are not in the Copy
    private static void missingFromCopy(ArrayList listA, String path, String LogPath, ArrayList MarkedForRemoval) throws IOException {
        String files;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        String NameExists = LogPath + "Up to date files.txt";
        PrintWriter OutExists;
        OutExists = new PrintWriter(NameExists);

        for (File listOfFile : listOfFiles != null ? listOfFiles : new File[0]) {
            if (listOfFile.isFile()) {
                files = listOfFile.getName();
                if (listA.contains(files)) {
                    OutExists.print(files + "\n");
                } else {
                    MarkedForRemoval.add(files);
                }
            }
        }
        OutExists.close();
    }

    //Generate list of new files in Copy and add them to the Instance
    private static void addToInstance(ArrayList listB, String CloudPath, String InstancePath, String LogPath, ArrayList NewFiles) throws IOException {
        String files;
        File folder = new File(CloudPath);
        File[] listOfFiles = folder.listFiles();
        String NameMissing = LogPath + "New files.txt";
        PrintWriter OutMissing;
        OutMissing = new PrintWriter(NameMissing);

        for (File listOfFile : listOfFiles != null ? listOfFiles : new File[0]) {
            if (listOfFile.isFile()) {
                files = listOfFile.getName();
                if (listB.contains(files)) {
                    //Do nothing
                } else {
                    OutMissing.print(files + "\n");
                    CopyOption[] options = new CopyOption[]{
                            StandardCopyOption.REPLACE_EXISTING,
                            StandardCopyOption.COPY_ATTRIBUTES
                    };

                    Path From = Paths.get(CloudPath + "\\" + files);
                    Path To = Paths.get(InstancePath + "\\" + files);
                    Files.copy(From, To, options);
                    System.out.println();
                    System.out.println("Added : " + files);
                    NewFiles.add(files);

                }
            }
        }
        OutMissing.close();

    }

    //Remove files
    private static void fileRemoval(String instancePath, String logPath, ArrayList approved, ArrayList markedForRemoval, ArrayList removalApproved, String LogPath) throws IOException {
        FileInputStream fis = new FileInputStream(logPath + "Clientsides.txt");

        //Construct BufferedReader from InputStreamReader
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        String line = "";
        while ((line = br.readLine()) != null) approved.add(line);
        br.close();


        for (Object aMarkedForRemoval : markedForRemoval) {
            if (approved.contains(aMarkedForRemoval)) {
                //Do nothing
            } else removalApproved.add(aMarkedForRemoval);
        }

        String myFileName = LogPath + "Removed files.txt";

        PrintWriter myOutFile;
        myOutFile = new PrintWriter(myFileName);

        for (Object aRemovalApproved : removalApproved) {
            Files.delete(Paths.get(instancePath + "\\" + aRemovalApproved));
            System.out.println();
            System.out.println("Removed : " + aRemovalApproved);
            myOutFile.print(aRemovalApproved + "\n");
        }

        myOutFile.close();
    }
}