package com.stonewaves.updater;

import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;

public class ModsCode {

    private static String LogPath = ".\\LogFiles\\Mods\\";

    //All the Array Lists
    private static ArrayList CloudFiles = new ArrayList();
    private static ArrayList InstanceFiles = new ArrayList();
    private static ArrayList Approved = new ArrayList();
    private static ArrayList MarkedForRemoval = new ArrayList();
    private static ArrayList RemovalApproved = new ArrayList();
    private static ArrayList NewFiles = new ArrayList();


    //Calls all subsequent methods
    public static void runMod(String CloudPath, String InstancePath) throws IOException {
        //Check for log Directory & create if it doesn't exist
        findDirectory();

        //--------------------------------------------------------------------------------------------------------------

        //Add files from Copy to the 'CloudFiles' Array List
        cloud(CloudFiles, CloudPath);

        //--------------------------------------------------------------------------------------------------------------

        //Add files from the selected to the 'InstanceFiles' Array List
        instance(InstanceFiles, InstancePath);

        //--------------------------------------------------------------------------------------------------------------

        //Generate a list of up-to-date file along with those that are not in the Cloud
        missingFromCopy(CloudFiles, InstancePath, LogPath, MarkedForRemoval);

        //--------------------------------------------------------------------------------------------------------------

        //Generate list of new files in Cloud
        addToInstance(InstanceFiles, CloudPath, InstancePath, LogPath, NewFiles);

        //--------------------------------------------------------------------------------------------------------------

        //Remove files
        fileRemoval(InstancePath, LogPath, Approved, MarkedForRemoval, RemovalApproved, LogPath);
    }

    //Check for log Directory & create if it doesn't exist
    public static void findDirectory() throws IOException {
        //Directory creation
        //File dir = new File(".\\LogFiles\\");
        new File(".\\LogFiles\\").mkdirs();
        new File(".\\LogFiles\\Mods\\").mkdirs();
        new File(".\\LogFiles\\Configs\\").mkdirs();

        //txt file creation
        new File(".\\LogFiles\\Mods\\Clientsides.txt").createNewFile();
        new File(".\\LogFiles\\Mods\\New files.txt").createNewFile();
        new File(".\\LogFiles\\Mods\\Up to date files.txt").createNewFile();
        new File(".\\LogFiles\\Mods\\Removed files.txt").createNewFile();

        new File(".\\LogFiles\\Configs\\Updated.txt").createNewFile();
        new File(".\\LogFiles\\Configs\\Up to date.txt").createNewFile();

    }


    //Add files from Copy to the 'CloudFiles' Array List
    private static void cloud(ArrayList Cloudlist, String path) {
        String files;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (File listOfFile : listOfFiles != null ? listOfFiles : new File[0]) {
            if (listOfFile.isFile()) {
                files = listOfFile.getName();
                System.out.println("Cloud - " + files);
                Cloudlist.add(files);
            }
        }
        System.out.println(Cloudlist.size() + " file(s) in location cloud folder");
    }

    //Add files from the selected to the 'InstanceFiles' Array List
    private static void instance(ArrayList InstanceList, String path) {
        String files;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (File listOfFile : listOfFiles != null ? listOfFiles : new File[0]) {
            if (listOfFile.isFile()) {
                files = listOfFile.getName();
                System.out.println("Instance - " + files);
                InstanceList.add(files);
            }
        }
        System.out.println(InstanceList.size() + " file(s) in location instance folder");
    }

    //Generate a list of up-to-date file along with those that are not in the Cloud
    private static void missingFromCopy(ArrayList listA, String path, String LogPath, ArrayList MarkedForRemoval) throws IOException {
        String files;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        String NameExists = LogPath + "Up to date files.txt";
        PrintWriter OutExists;
        OutExists = new PrintWriter(NameExists);

        for (File listOfFile : listOfFiles != null ? listOfFiles : new File[0]) {
            if (listOfFile.isFile()) {
                files = listOfFile.getName();
                if (listA.contains(files)) {
                    OutExists.print(files + "\n");
                } else {
                    MarkedForRemoval.add(files);
                }
            }
        }
        OutExists.close();
    }

    //Generate list of new files in Cloud and add them to the Instance
    private static void addToInstance(ArrayList listB, String CloudPath, String InstancePath, String LogPath, ArrayList NewFiles) throws IOException {
        String files;
        File folder = new File(CloudPath);
        File[] listOfFiles = folder.listFiles();
        String NameMissing = LogPath + "New files.txt";
        PrintWriter OutMissing;
        OutMissing = new PrintWriter(NameMissing);

        for (File listOfFile : listOfFiles != null ? listOfFiles : new File[0]) {
            if (listOfFile.isFile()) {
                files = listOfFile.getName();
                if (listB.contains(files)) {
                    //Do nothing
                } else {
                    OutMissing.print(files + "\n");
                    CopyOption[] options = new CopyOption[]{
                            StandardCopyOption.REPLACE_EXISTING,
                            StandardCopyOption.COPY_ATTRIBUTES
                    };

                    Path From = Paths.get(CloudPath + "\\" + files);
                    Path To = Paths.get(InstancePath + "\\" + files);
                    Files.copy(From, To, options);
                    System.out.println();
                    System.out.println("Added : " + files);
                    NewFiles.add(files);

                }
            }
        }
        OutMissing.close();

    }

    //Remove files that are not in cloud folder
    private static void fileRemoval(String instancePath, String logPath, ArrayList approved, ArrayList markedForRemoval, ArrayList removalApproved, String LogPath) throws IOException {
        FileInputStream fis = new FileInputStream(logPath + "Clientsides.txt");

        //Construct BufferedReader from InputStreamReader
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        String line = "";
        while ((line = br.readLine()) != null) approved.add(line);
        br.close();


        for (Object aMarkedForRemoval : markedForRemoval) {
            if (approved.contains(aMarkedForRemoval)) {
                //Do nothing
            } else removalApproved.add(aMarkedForRemoval);
        }

        //file name
        String myFileName = LogPath + "Removed files.txt";
        //file writwer
        PrintWriter myOutFile;
        myOutFile = new PrintWriter(myFileName);

        for (Object aRemovalApproved : removalApproved) {
            Files.delete(Paths.get(instancePath + "\\" + aRemovalApproved));
            System.out.println();
            System.out.println("Removed : " + aRemovalApproved);
            myOutFile.print(aRemovalApproved + "\n");
        }

        myOutFile.close();
    }


}
