# StoneWaves' Client Updater

StoneWaves' Updater - 0.1.jar 
-----------------------------
Release date : 14/01/2014 10:26 GMT

First development version (Rather broken).


StoneWaves' Updater - 0.2.jar
-----------------------------
Release date : 14/01/2014 13:03 GMT

- Added Log File generation and access from the GUI.
- Fixed some formatting in log files.

File path bugs reported.


StoneWaves' Updater - 0.3.jar
-----------------------------
Release date : 14/01/2014 17:07 GMT

- Fixed file creation code that was causing 0.1 & 0.2 to fail if folder structure didnt exist


StoneWaves' Updater - 0.4.jar
-----------------------------
Release date : TBC

- Config updates added

